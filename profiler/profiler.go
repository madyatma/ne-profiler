package profiler

import (
	"fmt"
	"time"
	"bytes"
	// "net/http"
	"runtime/pprof"
	"io/ioutil"
)


var (
	xxx = 5 * time.Second
)

type Config struct {
	Service string
	ServiceVersion string
	APIAddr string
	Xxx time.Duration
}

func Start(cfg Config) {
	if cfg.Xxx > 0 {
		xxx = cfg.Xxx * time.Second
	}

	for {
		start()
		time.Sleep(xxx)
	}
}

func start() {
	var prof bytes.Buffer
	fmt.Printf("asusususu%s\n", time.Now())
	// if err := pprof.StartCPUProfile(&prof); err != nil {
	// 	fmt.Printf("failed to write heap profile: %v", err)
	// 	return
	// }
	// pprof.StopCPUProfile()

	if err := pprof.Lookup("heap").WriteTo(&prof, 0); err != nil {
		fmt.Printf("failed to write heap profile: %v", err)
		return
	}

	if err := ioutil.WriteFile("wekeke", prof.Bytes(), 0644); err != nil {
		fmt.Printf("failed to write profile to file: %v", err)
		return
	}

	fmt.Printf("%s\n", prof.Bytes())
	//fmt.Printf("%s\n", pprof.Lookup("block"))
}

func Starting(cfg Config) {
	go Start(cfg)
}